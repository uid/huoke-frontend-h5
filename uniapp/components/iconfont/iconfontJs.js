export default {
  name: 'iconfontJs',
  render(createElement) {
  	return createElement('script', {
  		attrs: {
  			type: 'text/javascript',
  			src: '../../static/js/ali-font.js'
  		}
  	});
  }
}